package cz.muni.ics.ga4gh.web.controllers;

import static cz.muni.ics.ga4gh.web.configuration.security.PassportEndpointV1_2WebSecurityConfigurer.PATH_V_1_2;

import cz.muni.ics.ga4gh.base.model.Ga4ghTokenAuthorization;
import cz.muni.ics.ga4gh.middleware.facade.Ga4ghBrokerFacade;
import cz.muni.ics.ga4gh.middleware.service.exception.UserNotFoundException;
import cz.muni.ics.ga4gh.middleware.service.exception.UserNotUniqueException;
import cz.muni.ics.ga4gh.web.configuration.security.PassportEndpointV1_2WebSecurityConfigurer;
import cz.muni.ics.ga4gh.web.exception.InvalidRequestParametersException;
import cz.muni.ics.ga4gh.web.exception.MissingFieldException;
import java.security.Principal;
import java.text.ParseException;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.http.MediaType;
import org.springframework.security.oauth2.server.resource.authentication.BearerTokenAuthentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@ConditionalOnBean(PassportEndpointV1_2WebSecurityConfigurer.class)
@RestController
public class Ga4ghBrokerV1_2Controller {

    private final Ga4ghBrokerFacade ga4ghBrokerFacade;

    @Autowired
    public Ga4ghBrokerV1_2Controller(Ga4ghBrokerFacade ga4ghBrokerFacade) {
        this.ga4ghBrokerFacade = ga4ghBrokerFacade;
    }

    @GetMapping(value = PATH_V_1_2, produces = MediaType.APPLICATION_JSON_VALUE)
    public String getGa4ghPassportClaim(@NonNull Principal p)
        throws UserNotFoundException, UserNotUniqueException, ParseException,
        InvalidRequestParametersException
    {
        Ga4ghTokenAuthorization tokenAuthorization;
        try {
            tokenAuthorization = new Ga4ghTokenAuthorization((BearerTokenAuthentication) p);
        } catch (MissingFieldException e) {
            throw new InvalidRequestParametersException(
                "Could not extract all information from the provided OAuth2 access token");
        }
        return ga4ghBrokerFacade.getGa4ghPassport(tokenAuthorization);
    }

}
