package cz.muni.ics.ga4gh.web.controllers;

import static cz.muni.ics.ga4gh.web.configuration.security.VisaWriterEndpointWebSecurityConfigurer.PATH_VISAS;

import cz.muni.ics.ga4gh.base.properties.VisaWritersProperties;
import cz.muni.ics.ga4gh.middleware.service.impl.LocallyStoredVisasManagerServiceImpl;
import cz.muni.ics.ga4gh.web.requests.VisaWriteRequestBody;
import javax.validation.constraints.NotNull;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@ConditionalOnBean(value = VisaWritersProperties.class)
@RestController
public class VisaWriterController {

    private final LocallyStoredVisasManagerServiceImpl dbVisaManager;

    @Autowired
    public VisaWriterController(@NotNull LocallyStoredVisasManagerServiceImpl dbVisaManager) {
        this.dbVisaManager = dbVisaManager;
    }

    @PostMapping(value = PATH_VISAS + '/', consumes = MediaType.APPLICATION_JSON_VALUE)
    public void storeVisas(@NonNull @RequestBody VisaWriteRequestBody body) {
        dbVisaManager.persistVisas(body.getUsername(), body.getVisas());
    }

}
