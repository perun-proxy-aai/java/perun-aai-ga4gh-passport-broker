package cz.muni.ics.ga4gh.web.configuration.security.auth;

import cz.muni.ics.ga4gh.base.credentials.ApiKeyCredentials;
import java.util.Collection;
import javax.validation.constraints.NotNull;
import lombok.NonNull;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.Transient;

@Transient
public class ApiKeyAuthenticationToken extends AbstractAuthenticationToken {

    private final ApiKeyCredentials credentials;

    public ApiKeyAuthenticationToken(@NotNull ApiKeyCredentials credentials,
                                     @NonNull Collection<? extends GrantedAuthority> authorities)
    {
        super(authorities);
        this.credentials =credentials;
        setAuthenticated(true);
    }

    @Override
    public Object getCredentials() {
        return this.credentials.getApiKeyValue();
    }

    @Override
    public Object getPrincipal() {
        return this.credentials.getApiKeyHeader();
    }

}