package cz.muni.ics.ga4gh.web.configuration.properties;

import cz.muni.ics.ga4gh.base.credentials.BasicAuthCredentials;
import cz.muni.ics.ga4gh.base.exceptions.ConfigurationException;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;

@Getter
@ToString
@Slf4j
@Validated
@ConstructorBinding
@ConfigurationProperties(prefix = "passport-v1-0")
@ConditionalOnProperty(prefix = "passport-v1-0", value = "enabled", havingValue = "True")
public class V1_0EndpointProperties {

    @NotEmpty
    private final List<BasicAuthCredentials> credentials = new ArrayList<>();

    public V1_0EndpointProperties(@NotNull @NotEmpty List<BasicAuthCredentials> auth)
        throws ConfigurationException
    {
        for (BasicAuthCredentials c: auth) {
            if (!StringUtils.hasText(c.getUsername()) || !StringUtils.hasText(c.getPassword())) {
                throw new ConfigurationException("Invalid basic-auth credentials configured - empty username or password. Check your configuration.");
            }
        }
        this.credentials.addAll(auth);
    }

    @PostConstruct
    public void init() {
        log.info("Initialized '{}' properties", this.getClass().getSimpleName());
        log.debug("{}", this);
    }

}
