package cz.muni.ics.ga4gh.web.configuration.security;

import cz.muni.ics.ga4gh.base.credentials.BasicAuthCredentials;
import cz.muni.ics.ga4gh.web.configuration.properties.V1_0EndpointProperties;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.provisioning.InMemoryUserDetailsManagerConfigurer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

@ConditionalOnBean(V1_0EndpointProperties.class)
@Configuration
@Order(1)
public class PassportEndpointV1_0WebSecurityConfigurer {

    public static final String PATH_V_1_0 = "/1.0";

    private static final String ROLE_USER = "ROLE_USER";

    private final List<BasicAuthCredentials> basicAuthCredentialsList;

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public PassportEndpointV1_0WebSecurityConfigurer(
        V1_0EndpointProperties properties, PasswordEncoder passwordEncoder)
    {
        this.basicAuthCredentialsList = properties.getCredentials();
        this.passwordEncoder = passwordEncoder;
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        InMemoryUserDetailsManagerConfigurer<AuthenticationManagerBuilder> configurer = auth.inMemoryAuthentication();
        for (BasicAuthCredentials credentials: basicAuthCredentialsList) {
            configurer.withUser(credentials.getUsername())
                .password(passwordEncoder.encode(credentials.getPassword()))
                .authorities(ROLE_USER);
        }
    }

    @Bean
    public SecurityFilterChain basicAuthFilterChain(HttpSecurity http) throws Exception {
        http.antMatcher(PATH_V_1_0 + "/**")
            .authorizeRequests(configurer -> {
                configurer.antMatchers(PATH_V_1_0 + "/**").authenticated();
            })
            .httpBasic()
                .and()
            .sessionManagement(
                session -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
            )
            .exceptionHandling();

        http.headers().frameOptions().sameOrigin();
        return http.build();
    }

}
