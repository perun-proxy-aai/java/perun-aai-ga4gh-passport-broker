package cz.muni.ics.ga4gh.web.configuration.properties;

import cz.muni.ics.ga4gh.base.credentials.ApiKeyCredentials;
import cz.muni.ics.ga4gh.base.credentials.BasicAuthCredentials;
import java.util.ArrayList;
import java.util.List;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;

@Getter
@ToString
@Validated
@Slf4j
public class VisaWriterAuthProperties {

    @NotNull
    private final List<BasicAuthCredentials> basicAuth = new ArrayList<>();

    @NotNull
    private final List<ApiKeyCredentials> apiKeys = new ArrayList<>();

    public VisaWriterAuthProperties(List<BasicAuthCredentials> basicAuth,
                                    List<ApiKeyCredentials> apiKeys)
    {
        if (basicAuth != null) {
            this.basicAuth.addAll(basicAuth);
        }
        if (apiKeys != null) {
            this.apiKeys.addAll(apiKeys);
        }
    }

}
