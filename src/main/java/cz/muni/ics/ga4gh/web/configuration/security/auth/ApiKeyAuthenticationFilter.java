package cz.muni.ics.ga4gh.web.configuration.security.auth;

import static cz.muni.ics.ga4gh.web.configuration.security.VisaWriterEndpointWebSecurityConfigurer.ROLE_WRITER;

import cz.muni.ics.ga4gh.base.credentials.ApiKeyCredentials;
import java.io.IOException;
import java.util.List;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.StringUtils;

public class ApiKeyAuthenticationFilter implements Filter {

    private final List<ApiKeyCredentials> credentials;

    private static final GrantedAuthority WRITER_AUTHORITY = new SimpleGrantedAuthority(ROLE_WRITER);

    public static final List<GrantedAuthority> AUTHORITIES = List.of(WRITER_AUTHORITY);

    public ApiKeyAuthenticationFilter(List<ApiKeyCredentials> credentials) {
        this.credentials = credentials;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
        throws IOException, ServletException
    {
        if (request instanceof HttpServletRequest && response instanceof HttpServletResponse) {
            ApiKeyCredentials apiKey = getApiKey((HttpServletRequest) request);
            if (apiKey != null) {
                ApiKeyAuthenticationToken
                    apiToken = new ApiKeyAuthenticationToken(apiKey, AUTHORITIES);
                SecurityContextHolder.getContext().setAuthentication(apiToken);
            }
        }
        chain.doFilter(request, response);
    }

    private ApiKeyCredentials getApiKey(HttpServletRequest httpRequest) {
        for (ApiKeyCredentials credential: credentials) {
            String authHeader = httpRequest.getHeader(credential.getApiKeyHeader());
            if (authHeader != null) {
                if (StringUtils.hasText(authHeader) && credential.getApiKeyValue().equals(authHeader)) {
                    return credential;
                }
            }
        }

        return null;
    }
}