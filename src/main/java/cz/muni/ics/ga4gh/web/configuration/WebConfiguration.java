package cz.muni.ics.ga4gh.web.configuration;

import cz.muni.ics.ga4gh.base.properties.VisaWritersProperties;
import cz.muni.ics.ga4gh.web.configuration.properties.VisaWriterAuthProperties;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableWebSecurity
@Configuration
public class WebConfiguration {
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    @ConditionalOnBean(VisaWritersProperties.class)
    public VisaWriterAuthProperties visaWriterAuthProperties(
        VisaWritersProperties visaWritersProperties)
    {
        return new VisaWriterAuthProperties(
            visaWritersProperties.getBasicAuth(), visaWritersProperties.getApiKeys()
        );
    }

}
