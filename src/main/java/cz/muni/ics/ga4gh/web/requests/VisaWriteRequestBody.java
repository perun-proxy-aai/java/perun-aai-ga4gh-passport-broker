package cz.muni.ics.ga4gh.web.requests;

import java.util.Set;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Valid
public class VisaWriteRequestBody {

    @NotBlank
    private String username;

    @NotNull
    private Set<String> visas;

}
