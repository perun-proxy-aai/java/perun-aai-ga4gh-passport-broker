package cz.muni.ics.ga4gh;

import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationPropertiesScan
public class ApplicationConfiguration {

}
