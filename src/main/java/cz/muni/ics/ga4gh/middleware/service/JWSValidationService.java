package cz.muni.ics.ga4gh.middleware.service;

import com.nimbusds.jwt.SignedJWT;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import lombok.NonNull;

public interface JWSValidationService {

    void loadRemoteJwkSet(@NonNull URI jku) throws MalformedURLException;

    void loadRemoteJwkSet(@NonNull URL jku);


    boolean validateJwt(@NonNull SignedJWT jwt);


}
