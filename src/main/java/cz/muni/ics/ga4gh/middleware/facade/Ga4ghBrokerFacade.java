package cz.muni.ics.ga4gh.middleware.facade;

import com.fasterxml.jackson.databind.JsonNode;
import cz.muni.ics.ga4gh.base.model.Ga4ghTokenAuthorization;
import cz.muni.ics.ga4gh.middleware.service.exception.UserNotFoundException;
import cz.muni.ics.ga4gh.middleware.service.exception.UserNotUniqueException;
import java.text.ParseException;
import lombok.NonNull;

public interface Ga4ghBrokerFacade {

    String getGa4ghPassport(@NonNull Ga4ghTokenAuthorization ga4ghAccessToken)
        throws UserNotFoundException, UserNotUniqueException, ParseException;

    JsonNode getGa4ghPassportClaim(@NonNull String userIdentifier)
        throws UserNotFoundException, UserNotUniqueException;

}
