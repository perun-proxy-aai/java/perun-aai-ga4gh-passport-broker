package cz.muni.ics.ga4gh.middleware.broker.impl;

import static cz.muni.ics.ga4gh.base.model.Ga4ghPassportVisa.BY_SYSTEM;

import com.fasterxml.jackson.core.JsonProcessingException;
import cz.muni.ics.ga4gh.base.Utils;
import cz.muni.ics.ga4gh.base.model.ClaimRepositoryHeader;
import cz.muni.ics.ga4gh.base.model.Ga4ghClaimRepository;
import cz.muni.ics.ga4gh.base.model.Ga4ghPassportVisa;
import cz.muni.ics.ga4gh.base.model.VisaV1Type;
import cz.muni.ics.ga4gh.base.properties.Ga4ghClaimRepositoryProperties;
import cz.muni.ics.ga4gh.base.properties.MockBrokerProperties;
import cz.muni.ics.ga4gh.middleware.broker.MockOidcGa4ghBroker;
import cz.muni.ics.ga4gh.middleware.broker.helper.ExternalRepositoryUtils;
import cz.muni.ics.ga4gh.middleware.broker.helper.PassportAssemblyContext;
import cz.muni.ics.ga4gh.middleware.broker.helper.VisaAssemblyParameters;
import cz.muni.ics.ga4gh.middleware.broker.helper.VisaCreatingUtils;
import cz.muni.ics.ga4gh.middleware.broker.helper.VisaDataInitializer;
import cz.muni.ics.ga4gh.middleware.service.JWSValidationService;
import cz.muni.ics.ga4gh.middleware.service.JWTSigningService;
import cz.muni.ics.ga4gh.persistence.database.entity.VisaEntity;
import cz.muni.ics.ga4gh.persistence.database.repository.VisaEntityRepository;
import cz.muni.ics.ga4gh.persistence.mockdata.VisaData;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.http.client.InterceptingClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
@Slf4j
@ConditionalOnBean(MockBrokerProperties.class)
public class MockOidcGa4ghBrokerImpl implements MockOidcGa4ghBroker {

    private final JWSValidationService validationService;

    private final JWTSigningService jwtSigningService;

    private final VisaEntityRepository visaEntityRepository;

    private final List<Ga4ghClaimRepository> claimRepositories = new ArrayList<>();

    private final MockBrokerProperties mockBrokerProperties;

    private final Map<String, Set<VisaData>> userVisasMap;

    @Autowired
    public MockOidcGa4ghBrokerImpl(@NonNull JWSValidationService validationService,
                                   @NonNull JWTSigningService jwtSigningService,
                                   @NonNull MockBrokerProperties mockBrokerProperties,
                                   Optional<VisaEntityRepository> visaEntityRepository)
    {
        this.validationService = validationService;
        this.jwtSigningService = jwtSigningService;
        this.mockBrokerProperties = mockBrokerProperties;
        this.visaEntityRepository = visaEntityRepository.orElse(null);
        initializeClaimRepositories();
        this.userVisasMap = VisaDataInitializer.initialize(
            mockBrokerProperties.getVisaConfigsPath());
    }

    private void initializeClaimRepositories() {
        List<Ga4ghClaimRepositoryProperties> repoProps = mockBrokerProperties.getPassportRepositories();
        for (Ga4ghClaimRepositoryProperties ga4ghClaimRepositoryProperties: repoProps) {
            ExternalRepositoryUtils.initializeClaimRepository(
                ga4ghClaimRepositoryProperties,
                claimRepositories,
                mockBrokerProperties.getPassportRepositoriesTimeout()
            );
            ExternalRepositoryUtils.initializeRepositoryValidationKeys(
                ga4ghClaimRepositoryProperties.getName(),
                ga4ghClaimRepositoryProperties.getJwks(),
                validationService
            );
        }
    }

    @Override
    public List<Ga4ghPassportVisa> constructGa4ghPassportVisas(String userId) {
        List<Ga4ghPassportVisa> controlledAccessGrantsFromRepositories = new ArrayList<>();
        Set<String> linkedIdentities = new HashSet<>();

        ExternalRepositoryUtils.callExternalRepositories(
            userId,
            claimRepositories,
            controlledAccessGrantsFromRepositories,
            linkedIdentities,
            validationService
        );

        PassportAssemblyContext ctx = PassportAssemblyContext.builder()
            .subject(userId)
            .now(Instant.now().getEpochSecond())
            .controlledAccessGrants(controlledAccessGrantsFromRepositories)
            .linkedIdentities(linkedIdentities)
            .build();

        ctx.getResultVisas().addAll(fetchLocallyStoredVisas(userId, linkedIdentities));
        ctx.getResultVisas().addAll(ctx.getControlledAccessGrants());

        addConfiguredVisas(ctx);
        addLinkedIdentities(ctx);

        return ctx.getResultVisas();
    }

    private void addConfiguredVisas(@NonNull PassportAssemblyContext ctx) {
        Set<VisaData> dataSet = userVisasMap.getOrDefault(ctx.getSubject(), new HashSet<>());
        for (VisaData data: dataSet) {
            ctx.getResultVisas().add(
                VisaCreatingUtils.createVisa(
                    VisaAssemblyParameters.builder()
                        .issuer(mockBrokerProperties.getIssuer())
                        .jku(mockBrokerProperties.getJku())
                        .type(VisaV1Type.lookup(data.getType()))
                        .sub(ctx.getSubject())
                        .value(data.getValue())
                        .source(data.getSource())
                        .by(data.getBy())
                        .asserted(data.getAsserted())
                        .expires(data.getExpires())
                        .conditions(data.getConditions())
                        .jwtService(jwtSigningService)
                        .signer(mockBrokerProperties.getIssuer())
                        .build()
                    )
            );
        }
    }


    private void addLinkedIdentities(@NonNull PassportAssemblyContext ctx) {
        for (String linkedIdentity: ctx.getLinkedIdentities()) {
            ctx.getResultVisas().add(
                VisaCreatingUtils.createVisa(
                    VisaAssemblyParameters.builder()
                        .issuer(mockBrokerProperties.getIssuer())
                        .jku(mockBrokerProperties.getJku())
                        .type(VisaV1Type.LINKED_IDENTITIES)
                        .sub(ctx.getSubject())
                        .value(linkedIdentity)
                        .source(mockBrokerProperties.getIssuer())
                        .by(BY_SYSTEM)
                        .asserted(ctx.getNow())
                        .expires(Utils.getOneYearExpires(ctx.getNow()))
                        .jwtService(jwtSigningService)
                        .signer(mockBrokerProperties.getIssuer())
                        .build()
                )
            );
        }
    }

    protected List<Ga4ghPassportVisa> fetchLocallyStoredVisas(@NonNull String communityIdentifier,
                                                              @NonNull Set<String> linkedIdentities)
    {
        List<Ga4ghPassportVisa> localVisas = new ArrayList<>();
        if (visaEntityRepository != null) {
            Set<VisaEntity> visas = visaEntityRepository.findAllByUserIdAndExpAfter(
                communityIdentifier, LocalDateTime.now());
            if (visas != null && !visas.isEmpty()) {
                for (VisaEntity visa: visas) {
                    Ga4ghPassportVisa parsedVisa;
                    try {
                        parsedVisa = Utils.parseVisa(visa.getJwt());
                    } catch (JsonProcessingException | ParseException e) {
                        log.warn("Could not parse visa from '{}'", visa);
                        continue;
                    }

                    Utils.verifyVisa(parsedVisa, validationService);
                    if (!parsedVisa.isVerified()) {
                        log.warn("Could not verify visa '{}'", visa);
                        continue;
                    }
                    localVisas.add(parsedVisa);
                    linkedIdentities.add(visa.getLinkedIdentity());
                }
            }
        }
        return localVisas;
    }

}
