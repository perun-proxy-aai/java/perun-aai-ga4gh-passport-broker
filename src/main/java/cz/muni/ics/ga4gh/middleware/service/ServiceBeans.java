package cz.muni.ics.ga4gh.middleware.service;

import cz.muni.ics.ga4gh.base.properties.BrokersProperties;
import cz.muni.ics.ga4gh.base.properties.MockBrokerProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;

@Configuration
public class ServiceBeans {

    @Autowired
    @Bean
    @ConditionalOnBean(BrokersProperties.class)
    public Resource jwkFileResource(BrokersProperties brokersProperties) {
        return brokersProperties.getJwkKeystoreFile();
    }

    @Bean
    @ConditionalOnBean(MockBrokerProperties.class)
    public Resource jwkFileResourceMock(MockBrokerProperties properties) {
        return properties.getJwkKeystoreFile();
    }

}
