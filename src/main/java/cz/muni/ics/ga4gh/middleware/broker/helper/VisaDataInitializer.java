package cz.muni.ics.ga4gh.middleware.broker.helper;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import cz.muni.ics.ga4gh.persistence.mockdata.UserVisaData;
import cz.muni.ics.ga4gh.persistence.mockdata.VisaData;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

public class VisaDataInitializer {

    public static Map<String, Set<VisaData>> initialize(String dirPath) {
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory())
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        try (Stream<Path> paths = Files.walk(Paths.get(dirPath))) {
            Map<String, Set<VisaData>> result = new HashMap<>();
            paths.filter(Files::isRegularFile)
                    .map(path -> {
                        try {
                            return mapper.readValue(path.toFile(), UserVisaData.class);
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                    }).forEach(vd -> result.put(vd.getUserId(), vd.getVisas()));
            return result;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
