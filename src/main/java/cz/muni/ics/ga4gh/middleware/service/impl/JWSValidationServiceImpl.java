package cz.muni.ics.ga4gh.middleware.service.impl;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.RemoteKeySourceException;
import com.nimbusds.jose.crypto.RSASSAVerifier;
import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.JWKMatcher;
import com.nimbusds.jose.jwk.JWKSelector;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jose.jwk.source.RemoteJWKSet;
import com.nimbusds.jose.proc.SecurityContext;
import com.nimbusds.jwt.SignedJWT;
import cz.muni.ics.ga4gh.middleware.service.JWSValidationService;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class JWSValidationServiceImpl implements JWSValidationService {

    @Getter
    private final Map<URI, RemoteJWKSet<SecurityContext>> remoteJwkSets = new HashMap<>();

    @Override
    public void loadRemoteJwkSet(@NonNull URI jku) {
        try {
           loadRemoteJwkSet(jku.toURL(), jku);
        } catch (MalformedURLException ex) {
            log.warn("JKU '{}' is a malformed URL, cannot validate JWT", jku, ex);
        }
    }

    @Override
    public void loadRemoteJwkSet(@NonNull URL jku) {
        try {
            loadRemoteJwkSet(jku, jku.toURI());
        } catch (URISyntaxException ex) {
            log.warn("JKU '{}' is a malformed URI, cannot validate JWT", jku, ex);
        }
    }

    @Override
    public boolean validateJwt(SignedJWT jwt){
        URI jku = jwt.getHeader().getJWKURL();
        if (jku == null) {
            log.warn("JWT without JKU, cannot validate");
            return false;
        }
        if (!remoteJwkSets.containsKey(jku)) {
            loadRemoteJwkSet(jku);
        }
        RemoteJWKSet<SecurityContext> remoteJWKSet = remoteJwkSets.getOrDefault(jku, null);
        if (remoteJWKSet == null) {
            log.error("Trusted key sets does not contain JKU '{}', Visa verification failed", jku);
            return false;
        }

        try {
            List<JWK>keys = remoteJWKSet.get(new JWKSelector(
                new JWKMatcher.Builder().keyID(jwt.getHeader().getKeyID()).build()), null);
            try {
                RSASSAVerifier verifier = new RSASSAVerifier(((RSAKey) keys.get(0)).toRSAPublicKey());
                return jwt.verify(verifier);
            } catch (JOSEException ex) {
                log.warn("Cannot verify signature for JWT '{}'", jwt, ex);
                return false;
            }
        } catch (RemoteKeySourceException ex) {
            log.warn("Detected problem with remote keys for JWT '{}'", jwt, ex);
            return false;
        }
    }

    private void loadRemoteJwkSet(URL jkuUrl, URI jkuUri) {
        if (!remoteJwkSets.containsKey(jkuUri)) {
            remoteJwkSets.put(jkuUri, new RemoteJWKSet<>(jkuUrl));
        }
    }

}
