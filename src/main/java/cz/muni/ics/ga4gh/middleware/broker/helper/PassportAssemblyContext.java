package cz.muni.ics.ga4gh.middleware.broker.helper;

import cz.muni.ics.ga4gh.base.model.Affiliation;
import cz.muni.ics.ga4gh.base.model.Ga4ghPassportVisa;
import cz.muni.ics.ga4gh.persistence.perun.adapters.PerunAdapterRpc;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@Builder
public class PassportAssemblyContext {

    private long now;

    private PerunAdapterRpc perunAdapter;

    private String subject;

    private long perunUserId;

    @Builder.Default
    private Set<Affiliation> externalAffiliations = new HashSet<>();

    @Builder.Default
    private Set<Affiliation> localAffiliations = new HashSet<>();

    @Builder.Default
    private Set<String> linkedIdentities = new HashSet<>();

    @Builder.Default
    private List<Ga4ghPassportVisa> controlledAccessGrants = new ArrayList<>();

    @Builder.Default
    private List<Ga4ghPassportVisa> locallyStoredVisas = new ArrayList<>();

    @Builder.Default
    private final List<Ga4ghPassportVisa> resultVisas = new ArrayList<>();

}
