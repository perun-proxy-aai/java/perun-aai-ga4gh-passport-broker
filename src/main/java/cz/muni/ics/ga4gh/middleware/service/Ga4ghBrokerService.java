package cz.muni.ics.ga4gh.middleware.service;

import cz.muni.ics.ga4gh.base.model.Ga4ghPassport;
import cz.muni.ics.ga4gh.base.model.Ga4ghPassportClaim;
import cz.muni.ics.ga4gh.base.model.Ga4ghTokenAuthorization;
import cz.muni.ics.ga4gh.middleware.service.exception.UserNotFoundException;
import cz.muni.ics.ga4gh.middleware.service.exception.UserNotUniqueException;
import lombok.NonNull;

public interface Ga4ghBrokerService {

    Ga4ghPassportClaim getGa4ghPassportUserinfoStyle(@NonNull String userId)
        throws UserNotFoundException, UserNotUniqueException;

    Ga4ghPassport getGa4ghPassportTokenStyle(@NonNull Ga4ghTokenAuthorization accessToken)
        throws UserNotFoundException, UserNotUniqueException;

}
