package cz.muni.ics.ga4gh.middleware.service.impl;

import cz.muni.ics.ga4gh.base.Utils;
import cz.muni.ics.ga4gh.base.model.Ga4ghPassportVisa;
import cz.muni.ics.ga4gh.base.properties.VisaWritersProperties;
import cz.muni.ics.ga4gh.middleware.service.JWSValidationService;
import cz.muni.ics.ga4gh.middleware.service.LocallyStoredVisasManagerService;
import cz.muni.ics.ga4gh.persistence.database.entity.VisaEntity;
import cz.muni.ics.ga4gh.persistence.database.repository.VisaEntityRepository;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.validation.constraints.NotNull;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

@ConditionalOnBean(value = VisaWritersProperties.class)
@Service
@Slf4j
public class LocallyStoredVisasManagerServiceImpl implements LocallyStoredVisasManagerService {

    private final JWSValidationService validationService;

    private final VisaEntityRepository visaEntityRepository;

    @Autowired
    public LocallyStoredVisasManagerServiceImpl(@NonNull JWSValidationService validationService,
                                                @NonNull VisaEntityRepository visaEntityRepository)
    {
        this.validationService = validationService;
        this.visaEntityRepository = visaEntityRepository;
    }

    @Override
    public void persistVisas(@NotNull String username, @NotNull Set<String> visas) {
        Set<VisaEntity> toBeStored = new HashSet<>();
        String source = null;
        for (String visa: visas) {
            Ga4ghPassportVisa parsedVisa = Utils.parseVisa(visa);
            if (parsedVisa == null) {
                log.warn("Could not parse visa from '{}'", visa);
                continue;
            }
            if (source == null) {
                source = parsedVisa.getIss();
            }
            Utils.verifyVisa(parsedVisa, validationService);
            if (parsedVisa.isVerified()) {
                toBeStored.add(new VisaEntity(
                    null, username, parsedVisa.getIss(), parsedVisa.getLinkedIdentity(),
                    parsedVisa.getExp(), parsedVisa.getJwt()
                ));
            }
        }
        if (toBeStored.isEmpty()) {
            return;
        }
        Set<VisaEntity> visasToBeRemoved = visaEntityRepository.findAllByUserIdAndSource(
            username, source);
        List<VisaEntity> stored = visaEntityRepository.saveAll(toBeStored);
        if (stored.size() == toBeStored.size()) {
            visaEntityRepository.deleteAll(visasToBeRemoved);
        }
    }

}
