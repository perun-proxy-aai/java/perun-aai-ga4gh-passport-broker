package cz.muni.ics.ga4gh.base.model;

import cz.muni.ics.ga4gh.web.exception.MissingFieldException;
import java.util.List;
import java.util.Map;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;
import org.springframework.security.oauth2.server.resource.authentication.BearerTokenAuthentication;

@Getter
@Setter
@ToString
@EqualsAndHashCode
public class Ga4ghTokenAuthorization {

    public static final String TOKEN_ATTR_SUB = "sub";
    public static final String TOKEN_ATTR_USERNAME = "username";
    public static final String TOKEN_ATTR_AUD = "aud";

    private String sub;

    private List<String> aud;

    private String bearerToken;

    public Ga4ghTokenAuthorization(@NonNull BearerTokenAuthentication authentication) {
        Map<String, Object> attributes = authentication.getTokenAttributes();
        if (!attributes.containsKey(TOKEN_ATTR_SUB)) {
            throw new MissingFieldException("Missing field '" + TOKEN_ATTR_SUB + "' in the token");
        } else if (!attributes.containsKey(TOKEN_ATTR_AUD)) {
            throw new MissingFieldException("Missing field  '" + TOKEN_ATTR_AUD + "' in the token");
        }

        this.sub = (String) attributes.get(TOKEN_ATTR_SUB);
        this.aud = (List<String>) attributes.get(TOKEN_ATTR_AUD);
        this.bearerToken = authentication.getToken().getTokenValue();
    }

}
