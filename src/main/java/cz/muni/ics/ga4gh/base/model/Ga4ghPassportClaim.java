package cz.muni.ics.ga4gh.base.model;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode
public class Ga4ghPassportClaim {
    public static final String GA4GH_PASSPORT_V1 = "ga4gh_passport_v1";

    private final List<Ga4ghPassportVisa> ga4ghPassportVisaV1 = new ArrayList<>();

    public void addVisas(List<Ga4ghPassportVisa> visas) {
        if (visas == null || visas.isEmpty()) {
            return;
        }
        this.ga4ghPassportVisaV1.addAll(visas);
    }

    public List<String> serialize() {
        List<String> serializedVisas = new ArrayList<>();
        for (Ga4ghPassportVisa visa: ga4ghPassportVisaV1) {
            serializedVisas.add(visa.serialize());
        }
        return serializedVisas;
    }

    public ArrayNode asClaim() {
        ArrayNode ga4ghPassportV1 = JsonNodeFactory.instance.arrayNode();
        if (!ga4ghPassportVisaV1.isEmpty()) {
            for (Ga4ghPassportVisa visa: ga4ghPassportVisaV1) {
                ga4ghPassportV1.add(visa.serialize());
            }
        }
        return ga4ghPassportV1;
    }

    public LocalDateTime getFirstVisaExp() {
        LocalDateTime firstExp = null;
        for (Ga4ghPassportVisa visa: ga4ghPassportVisaV1) {
            LocalDateTime visaExp = visa.getExp();
            if (visaExp == null) {
                continue;
            }
            if (firstExp == null) {
                firstExp = visaExp;
            } else if (firstExp.isAfter(visaExp)) {
                firstExp = visaExp;
            }
        }
        return firstExp;
    }

}
