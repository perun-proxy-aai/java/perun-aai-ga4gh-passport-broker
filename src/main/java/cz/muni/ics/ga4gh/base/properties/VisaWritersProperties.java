package cz.muni.ics.ga4gh.base.properties;

import cz.muni.ics.ga4gh.base.credentials.ApiKeyCredentials;
import cz.muni.ics.ga4gh.base.credentials.BasicAuthCredentials;
import cz.muni.ics.ga4gh.base.model.VisaWriterConfiguration;
import cz.muni.ics.ga4gh.persistence.database.properties.DatabaseProperties;
import java.util.ArrayList;
import java.util.List;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;
import org.springframework.validation.annotation.Validated;

@Getter
@ToString
@Slf4j
@Validated
@ConstructorBinding
@ConfigurationProperties(prefix = "visa-writers")
@ConditionalOnProperty(prefix = "visa-writers", name = "enabled", havingValue = "True")
public class VisaWritersProperties implements InitializingBean {

    @NotNull
    private final List<BasicAuthCredentials> basicAuth = new ArrayList<>();

    @NotNull
    private final List<ApiKeyCredentials> apiKeys = new ArrayList<>();

    @NotNull
    private final DatabaseProperties databaseProperties;

    @Autowired
    public VisaWritersProperties(DatabaseProperties db, List<VisaWriterConfiguration> repositories) {
        this.databaseProperties = db;
        for (VisaWriterConfiguration configuration: repositories) {
            if (configuration.getAuth().getBasicAuth() != null) {
                this.basicAuth.add(configuration.getAuth().getBasicAuth());
            }
            if (configuration.getAuth().getApiKey() != null) {
                this.apiKeys.add(configuration.getAuth().getApiKey());
            }
        }
    }

    @Override
    public void afterPropertiesSet() {
        if (this.basicAuth.isEmpty() && this.apiKeys.isEmpty()) {
            log.warn("No credentials configured for Visa Writer endpoint");
        }
        log.info("Initialized '{}' properties", this.getClass().getSimpleName());
        log.debug("{}", this);
    }

}
