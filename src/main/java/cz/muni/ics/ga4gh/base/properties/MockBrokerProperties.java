package cz.muni.ics.ga4gh.base.properties;

import cz.muni.ics.ga4gh.base.exceptions.ConfigurationException;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraints.URL;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;
import org.springframework.core.io.FileUrlResource;
import org.springframework.validation.annotation.Validated;

@Getter
@ToString
@Slf4j

@Validated
@ConstructorBinding
@ConfigurationProperties("mock-broker")
@ConditionalOnProperty(prefix = "mock-broker", name = "enabled", havingValue = "True")
public class MockBrokerProperties {

    @URL
    private final String issuer;

    @NotNull
    private final URI jku;

    @NotNull
    private final FileUrlResource jwkKeystoreFile;

    private final String visaConfigsPath;

    private final Integer passportRepositoriesTimeout;

    @NotNull
    private final List<Ga4ghClaimRepositoryProperties> passportRepositories = new ArrayList<>();

    public MockBrokerProperties(@NonNull String issuer,
                                @NonNull String jku,
                                @NonNull String pathToJwkFile,
                                List<Ga4ghClaimRepositoryProperties> passportRepositories,
                                @NonNull String visaConfigsPath, Integer passportRepositoriesTimeout)
        throws MalformedURLException, ConfigurationException, URISyntaxException
    {
        this.issuer = issuer;
        this.visaConfigsPath = visaConfigsPath;
      this.passportRepositoriesTimeout = passportRepositoriesTimeout;
      File dir = new File(visaConfigsPath);
        if (!dir.exists()) {
            throw new ConfigurationException("provided path links to a non-existent location");
        } else if (!dir.isDirectory()) {
            throw new ConfigurationException("provided path does not link to a directory");
        }
        try {
            jwkKeystoreFile = new FileUrlResource(pathToJwkFile);
            if (!this.jwkKeystoreFile.exists()) {
                throw new Exception("JWK file does not exist");
            } else if (!this.jwkKeystoreFile.isReadable()) {
                throw new Exception("JWK file is not readable");
            }
            this.jwkKeystoreFile.getFile();
        } catch (Exception e) {
            throw new ConfigurationException("Error when loading JWK keystore file: " + e.getMessage());
        }
        this.jku = new java.net.URL(jku).toURI();
        this.passportRepositories.addAll(passportRepositories);
    }

    @PostConstruct
    public void init() {
        log.info("Initialized '{}' properties", this.getClass().getSimpleName());
        log.debug("{}", this);
    }

}
