package cz.muni.ics.ga4gh.base.model;

import cz.muni.ics.ga4gh.base.credentials.ApiKeyCredentials;
import cz.muni.ics.ga4gh.base.credentials.BasicAuthCredentials;
import cz.muni.ics.ga4gh.base.exceptions.ConfigurationException;
import java.net.MalformedURLException;
import java.net.URL;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.validation.annotation.Validated;

@Getter
@ToString
@EqualsAndHashCode
@Validated
public class VisaWriterConfiguration implements InitializingBean {

    @NotBlank
    private final String name;

    @NotBlank
    private final String jwks;

    @NotNull
    private final Auth auth;

    public VisaWriterConfiguration(String name, String jwks, Auth auth) {
        this.name = name;
        this.jwks = jwks;
        this.auth = auth;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        try {
            new URL(jwks);
        } catch (MalformedURLException ex) {
            throw new ConfigurationException("Invalid JWKS URL configured", ex);
        }
    }

    @Getter
    @ToString
    @EqualsAndHashCode
    @AllArgsConstructor
    @Validated
    public static final class Auth implements InitializingBean {
        private final BasicAuthCredentials basicAuth;

        private final ApiKeyCredentials apiKey;

        @Override
        public void afterPropertiesSet() throws Exception {
            if (basicAuth == null && apiKey == null) {
                throw new ConfigurationException(
                    "At least one authentication method has to be configured for visa writer"
                );
            }
        }
    }

}
