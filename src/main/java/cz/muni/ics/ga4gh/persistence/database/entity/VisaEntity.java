package cz.muni.ics.ga4gh.persistence.database.entity;

import com.nimbusds.jwt.SignedJWT;
import cz.muni.ics.ga4gh.persistence.database.converter.SignedJwtToStringConverter;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder
@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Valid
@Entity
@Table(name = "visas", indexes = {
    @Index(name = "idx_visaentity_user_id_source", columnList = "user_id,source")
})
public class VisaEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Column(name = "user_id", nullable = false)
    private String userId;

    @NotBlank
    @Column(name = "source", nullable = false, length = 1024)
    private String source;

    @NotBlank
    @Column(name = "linked_identity", nullable = false, length = 1024)
    private String linkedIdentity;

    @NotNull
    @Column(name = "exp", nullable = false)
    private LocalDateTime exp;

    @NotNull
    @Column(name = "jwt", nullable = false)
    @Convert(converter = SignedJwtToStringConverter.class)
    @Lob
    private SignedJWT jwt;

}
