package cz.muni.ics.ga4gh.persistence.perun.exception;

public class PerunEntityMissingFieldException extends RuntimeException {

    public PerunEntityMissingFieldException() {
        super();
    }

    public PerunEntityMissingFieldException(String s) {
        super(s);
    }

    public PerunEntityMissingFieldException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public PerunEntityMissingFieldException(Throwable throwable) {
        super(throwable);
    }

    protected PerunEntityMissingFieldException(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }
}
