package cz.muni.ics.ga4gh.persistence.database.converter;

import com.nimbusds.jwt.SignedJWT;
import java.text.ParseException;
import javax.persistence.AttributeConverter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

@Slf4j
public class SignedJwtToStringConverter implements AttributeConverter<SignedJWT, String> {

    @Override
    public String convertToDatabaseColumn(SignedJWT attribute) {
        if (attribute == null) {
            return null;
        }
        return attribute.serialize();
    }

    @Override
    public SignedJWT convertToEntityAttribute(String dbData) {
        if (!StringUtils.hasText(dbData)) {
            return null;
        }
        try {
            return SignedJWT.parse(dbData);
        } catch (ParseException ex) {
            log.error("Failed to parse as JWT '{}'", dbData, ex);
            return null;
        }
    }

}
