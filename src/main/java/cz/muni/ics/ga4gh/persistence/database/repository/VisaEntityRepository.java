package cz.muni.ics.ga4gh.persistence.database.repository;

import cz.muni.ics.ga4gh.base.properties.VisaWritersProperties;
import cz.muni.ics.ga4gh.persistence.database.entity.VisaEntity;
import java.time.LocalDateTime;
import java.util.Set;
import lombok.NonNull;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@ConditionalOnBean(value = VisaWritersProperties.class)
public interface VisaEntityRepository extends JpaRepository<VisaEntity, Long> {

    Set<VisaEntity> findAllByUserIdAndExpAfter(@NonNull String userId, @NonNull LocalDateTime now);

    void deleteAllByExpBefore(@NonNull LocalDateTime now);

    Set<VisaEntity> findAllByUserIdAndSource(@NonNull String userId, @NonNull String source);

}
