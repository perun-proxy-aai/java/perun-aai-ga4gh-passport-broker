package cz.muni.ics.ga4gh.persistence.perun.adapters;

import cz.muni.ics.ga4gh.base.model.Affiliation;
import cz.muni.ics.ga4gh.persistence.perun.model.UserExtSource;
import java.util.List;
import java.util.Set;

public interface PerunAdapterRpc {

    /**
     * Fetch user based on his principal (extLogin and extSource) from Perun
     *
     * @return PerunUser with id of found user
     */
    Long getPerunUserId(String extLogin, String extSourceName);

    boolean isUserInGroup(Long userId, Long groupId);

    List<Affiliation> getGroupAffiliations(Long userId, String groupAffiliationsAttr);

    List<Affiliation> getGroupAffiliations(Long userId, Long voId, String groupAffiliationsAttr);

    Set<Long> getUserIdsByAttributeValue(String attrName, String attrValue);

    String getUserSub(Long userId, String subAttribute);

    boolean isUserInVo(Long userId, Long voId);

    String getUserAttributeCreatedAt(Long userId, String attrName);

    Set<Affiliation> getUserExtSourcesAffiliations(Long userId, String affiliationsAttr);

    List<UserExtSource> getIdpUserExtSources(Long perunUserId);

    Set<Affiliation> getGroupAffiliations(Long userId,
                                          Long voId,
                                          Long rootGroupId,
                                          String affiliationsAttr,
                                          String orgUrlAttr,
                                          String groupMemberExpirationAttr);

}
