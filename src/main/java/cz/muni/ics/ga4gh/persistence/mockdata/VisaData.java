package cz.muni.ics.ga4gh.persistence.mockdata;

import com.fasterxml.jackson.databind.JsonNode;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.validation.annotation.Validated;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Validated
public class VisaData {

    @Min(1)
    private long expires = LocalDateTime.now().plusYears(1).toEpochSecond(ZoneOffset.UTC);

    // === mandatory ===
    @Min(0)
    private long asserted = LocalDateTime.now().minusYears(1).toEpochSecond(ZoneOffset.UTC);

    @NotBlank
    private String source;

    @NotNull
    private String type;

    @NotBlank
    private String value;


    // === optional ===
    private String by;

    private JsonNode conditions;

}
