package cz.muni.ics.ga4gh.persistence.database;

import cz.muni.ics.ga4gh.persistence.database.repository.VisaEntityRepository;
import java.time.LocalDateTime;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@ConditionalOnBean(value = VisaEntityRepository.class)
@Component
@EnableScheduling
public class VisaDatabaseStorageTaskScheduler {

    private static final long ONE_HOUR = 1000 * 60;

    private final VisaEntityRepository visaEntityRepository;

    @Autowired
    public VisaDatabaseStorageTaskScheduler(@NonNull VisaEntityRepository visaEntityRepository) {
        this.visaEntityRepository = visaEntityRepository;
    }

    @Scheduled(fixedDelay = ONE_HOUR, initialDelay = ONE_HOUR)
    public void cleanupExpiredVisas() {
        visaEntityRepository.deleteAllByExpBefore(LocalDateTime.now());
    }

}
