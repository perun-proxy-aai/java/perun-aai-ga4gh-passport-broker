## perun-aai-ga4gh-passport-broker

Implementation of an GA4GH Passport & Visa broker relying on Perun AAI and exposing its functionality as a REST API method.

## TODO

- refactor definition of writers into visa DB
  - add possibility to use OAuth tokens to write into the passport writer
- refactor the definition of credentials etc. per endpoint
- refactor working with JWKs and JWSes
- implement the enablement of v1.2 endpoint and the full stack based on this enablement
- implement the enablement of v1.0 endpoint and the full stack based on this enablement
